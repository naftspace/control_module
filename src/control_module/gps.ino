#ifdef USE_GPS
#include "TinyGPS++.h" // https://github.com/mikalhart/TinyGPSPlus


TinyGPSPlus gps;

void GpsInit() {
  GPS_SERIAL.begin(9600, SERIAL_8N1, PIN_GPS_RX, PIN_GPS_TX);
  Serial.println("Initializing gps...ok");
}

void GpsReadBuffer() {
  uint8_t temp;

  // バッファに何かあったら読み取り解析
  while (GPS_SERIAL.available()) {
    temp = GPS_SERIAL.read();
    //    Serial.print((char)temp);
    gps.encode(temp);
  }
}

// 緯度経度を読み取る関数
void GpsReadPosition(float *lati, float *lon) {
  GpsReadBuffer();
  *lati = gps.location.lat();
  *lon = gps.location.lng();
}
#endif
