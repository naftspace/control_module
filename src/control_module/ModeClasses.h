#ifndef MODE_BASE_HEADER_FILE
#define MODE_BASE_HEADER_FILE

#include "Utility.h"
#include "Parameters.h"
#include "PID.h"

// 状態の挙動を定義するためのクラスの基底クラス
class ModeBase {
  public:
    virtual ~ModeBase() {}

    // センサーの値から出力および内部状態を決定する
    virtual OutputInfo Control(const SensorInfo sensors) = 0;

    // 状態遷移直後に初期化を行うメソッド
    virtual void Reset() = 0;

    // 状態が遷移した時間をもつ関数
    void SetStartedTime(uint32_t time_ms) {
      started_time_ms_ = time_ms;
    }

  protected:
    // 状態が遷移した時間[ms]
    uint32_t started_time_ms_;
};

// サーボを開閉してパラシュートを格納したり動作確認を行う状態
class ModeSetting : ModeBase {
  public:
    ModeSetting() {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;

      outputs.target_torque = 0;
      outputs.angle = 0;
      outputs.next_mode = MODE_SETTING;

#ifdef ENABLE_TO_READY_BY_TIME
      //      Serial.println(del/ta_time);
      if (delta_time > TO_READY_TIME) {
        outputs.next_mode = MODE_READY;
      }
#endif

      return outputs;
    }

    void Reset() override {
    }

  private:
};

// 発射待機状態
class ModeReady : ModeBase {
  public:
    ModeReady() {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;

      // 高加速度を記録しているかどうかの判定を行う
      const float acc_sq_norm = sensors.ax * sensors.ax + sensors.ay * sensors.ay + sensors.az * sensors.az;
      const float threshold = IGNITION_THRESHOLD_ACCEL * IGNITION_THRESHOLD_ACCEL;
      const bool is_high_accel = acc_sq_norm > threshold;

      outputs.target_torque = 0;
      outputs.angle = 0;

      if ( is_high_accel && is_high_accel_before_ ) {
        // 何秒間しきい値を超えているかを計算
        const float delta_time = 1e-3 * (float)(sensors.time_ms - high_accel_started_time_);

        if ( IGNITION_THRESHOLD_TIME < delta_time ) {
          outputs.next_mode = MODE_BURNING;  // 長時間高加速度なので点火と判断
        } else {
          outputs.next_mode = MODE_READY;
        }
      } else if (is_high_accel) {
        // 高加速度が始まったので開始時刻を記録
        outputs.next_mode = MODE_READY;
        high_accel_started_time_ = sensors.time_ms;
      } else {
        outputs.next_mode = MODE_READY;
      }

      is_high_accel_before_ = is_high_accel;


      return outputs;
    }

    void Reset() override {
      is_high_accel_before_ = false;
    }
  private:

    bool is_high_accel_before_;         // 前回の処理で高加速度を記録した場合true
    uint32_t high_accel_started_time_;  // 高加速度を最初に記録した時間[ms]
};

// エンジンが燃焼している状態
class ModeBurning : ModeBase {
  public:
    ModeBurning() {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;
      const float delta_time = 1e-3 * (float)(sensors.time_ms - started_time_ms_);

      outputs.target_torque = 0;
      outputs.angle = 0;

      // BURNING_TIME [s]経過したら遷移
      if ( BURNING_TIME < delta_time ) {
        outputs.next_mode = MODE_ROLL;
      } else {
        outputs.next_mode = MODE_BURNING;
      }

      return outputs;
    }

    void Reset() override {

    }
  private:

};

// エンジン燃焼が終了し，放物線運動を始めた状態
// ロール制御を行う
class ModeRoll : ModeBase {
  public:
    ModeRoll() : pid_(GAIN_ROLL_KP, 0, GAIN_ROLL_KD) {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;

      outputs.next_mode = MODE_ROLL;
      outputs.target_torque = CalcTorque(sensors);
      outputs.angle = CalcAngle(outputs.target_torque, sensors.d_height);

      // MODE_PITCHへの遷移
      // TODO: 確認・修正(純粋関数じゃないのでややこしい)
      // satisfy_time_ = satisfy_cond_ ? satisfy_time_ : 0;
      // satisfy_cond_ = fabs(CalcError(sensors)) < 5.0;
      // if ( satisfy_cond_ ) {
      //   satisfy_time_ += sensors.dt_ms * 1e-3;
      // }
      //
      // if ( satisfy_time_ > 1.0 ) {
      //   outputs.next_mode = MODE_PITCH;
      // }

#ifndef DEBUG_MODE
      // 軸が大きく傾いたらMODE_ENDに遷移
      if ( sin(radians(sensors.pitch)) < sin(radians(90 - THRESHOLD_ANGLE_TO_STOP_CONTROL)) ) {
        outputs.next_mode = MODE_END;
      }
#endif

      // stop command
      if ( sensors.cmd == RESULT_STOP_CONTROL ) {
        outputs.next_mode = MODE_END;
      }

      return outputs;
    }

    float CalcTorque(const SensorInfo sensors) {
      const float error = radians(CalcError(sensors)); // rad
      const float torque = pid_.output(error, sensors.dt_ms * 1e-3);

      // pitch>80は制御を無効化する
      if ( sin(radians(sensors.pitch)) > sin(radians(80)) ) {
        return 0;
      }else{
        return torque;
      }
    }

    // deg
    float CalcError(const SensorInfo sensors) {
      const float target = cos(radians(sensors.roll)) > 0 ? 0 : 180;
      const float error = normalize_angle_deg(target - sensors.roll);

      return error;
    }

    // 速度とトルクからサーボの角度を計算する関数
    // torque[N m], velocity[m/s]
    float CalcAngle(const float torque, const float velocity) {
      const float kv2 = PARAM_ROLL_K * velocity * velocity;

      return torque / kv2;
    }

    void Reset() override {
      pid_.init();
      satisfy_time_ = 0;
      satisfy_cond_ = false;
    }

  private:
    PID pid_;
    bool satisfy_cond_;
    float satisfy_time_;
};

// エンジン燃焼が終了し，放物線運動をしている状態
// ピッチ制御を行う
class ModePitch : ModeBase {
  public:
    ModePitch() : pid_(GAIN_PITCH_KP, 0, GAIN_PITCH_KD) {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;
      const float delta_time = 1e-3 * (float)(sensors.time_ms - started_time_ms_);

      outputs.next_mode = MODE_PITCH;
      outputs.target_torque = CalcTorque(sensors);
      outputs.angle = CalcAngle(outputs.target_torque, sensors.d_height);

      // 軸が大きく傾いたらMODE_ENDに遷移
      if ( sin(radians(sensors.pitch)) < sin(radians(90 - THRESHOLD_ANGLE_TO_STOP_CONTROL)) ) {
        outputs.next_mode = MODE_END;
      }

      // stop command
      if ( sensors.cmd == RESULT_STOP_CONTROL ) {
        outputs.next_mode = MODE_END;
      }

      outputs.next_mode = MODE_END; // TODO: 実装してコメントアウト
      return outputs;
    }

    float CalcTorque(const SensorInfo sensors) {
      // TODO: implement

      float x = 0.25 * (90 - sensors.pitch);
      x *= sensors.roll < 0 ? -1 :  1;

      float v = sensors.d_height;
      float t = PARAM_PITCH_K * v * v * x;

      return t;
    }

    void Reset() override {
      pid_.init();
    }

    // 速度とトルクからサーボの角度を計算する関数
    // torque[N m], velocity[m/s]
    float CalcAngle(const float torque, const float velocity) {
      const float kv2 = PARAM_PITCH_K * velocity * velocity;

      return torque / kv2;
    }

  private:
    PID pid_;
};

// 制御終了した状態
class ModeEnd : ModeBase {
  public:
    ModeEnd() {
      Reset();
    }

    OutputInfo Control(const SensorInfo sensors) override {
      OutputInfo outputs;

      outputs.next_mode = MODE_END;
      outputs.target_torque = 0;
      outputs.angle = 0;

      return outputs;
    }

    void Reset() override {

    }
  private:

};

#endif
