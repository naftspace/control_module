
#include "SD_MMC.h"

// SDカードを開けたらtrue
bool is_sd_open = true;
// 保存するログデータの番号
int n_logdata = 0;
// ファイル名を格納する変数
char sd_filename[64];

void StorageInit() {
  Serial.print("Initializing SD card...");

  if (not SD_MMC.begin()) {
    // 初期化に失敗した場合
    Serial.println("ng");
    is_sd_open = false;
    ResetMicrocontroller(); // リセット
  } else {
    // 初期化に成功した場合は
    Serial.println("ok");
    is_sd_open = true;

    //ファイル名がかぶらないようなファイルを作る
    do {
      sprintf(sd_filename, "/%04d.txt", n_logdata);
      if ( SD_MMC.exists(sd_filename) == false )
        break;
      n_logdata++;
    } while (true);
  }
}

void StorageLoop(SensorInfo sensors, OutputInfo outputs) {
  // 書き込む文字列
  const String str = to_string(sensors) + to_string(outputs);

#ifdef DEBUG_MODE
  // 必要ならシリアルに表示
  // Serial.println(str);

  Serial.print("\"**CAUTION**:DEBUG_MODE\",");

  // ax,ay,az,gx,gy,gz,mx,my,mz,roll,pitch,yaw
  Serial.print(to_string(sensors, true));

  // next_mode,target_torque,servo_angle
  // Serial.print(to_string(outputs));

  Serial.println();

#endif

  if ( is_sd_open == false )
    return;

  // ファイルオープン
  File dataFile = SD_MMC.open(sd_filename, FILE_APPEND);

  if (dataFile) {
    // うまく開けていたら文字列を書き込む
    dataFile.println(str);
    dataFile.close();
    //        Serial.println("wrote");
  }
  else {
    //    Serial.println("error opening datalog.txt");
    //    Serial.println("miss");
  }
}
