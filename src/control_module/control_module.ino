#include <Wire.h>
#include "Utility.h"
#include "PinAssign.h"
#include "Parameters.h"
#include "LPF.h"
#include "DataBuffer.h"
#include "DataTransfer.h"
#include "MadgwickAHRSEx.h" //https://github.com/TonyMooori/MadgwickAHRSEx

// 気圧センサの出力値にかけるLPF
LPF lpf_pres(32);
// スリープ用変数
uint32_t old_time_ms;
// 点火検知時の高度
float height_at_ground;
// 1ループ前の高度
float old_height;
// シリアル通信でテストする時用のやつ
DataBuffer my_buff(16);
// Madgwickフィルタ
Madgwick filter;
// GPSデータを無線用マイコンに送信するためのもの
DataTransfer<GPSData> data_transfer(0);

void setup() {
  float temp;

  // 初期化されていない場合はLEDを消灯
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_PITOT_TUBE, INPUT);
  digitalWrite(PIN_LED, LOW);

  SerialSetup();
  Serial.println("initializing...");
  delay(100);

  // Wire初期化
  Wire.begin();
  delay(100);

  // 制御関係の初期化
  ControlInit();
  delay(100);

  // フィルタの初期化
  filter. begin(100);
  delay(100);

#ifdef USE_GPS
  // GPSの初期化
  GpsInit();
  delay(100);
#endif

  // ストレージの初期化
  StorageInit() ;
  delay(100);

  // imu関係の初期化
  ImuInit();
  delay(100);

  // 各状態の初期化
  ModeInit();
  delay(100);

  // このファイルで定義したグローバル変数の初期化
  old_time_ms = millis();
  height_at_ground = 0.0;
  ImuReadPressure(&temp);
  old_height = pressureToAltitudeMeters(temp, STANDARD_ATMOSPHERE);

  // 初期化されたことを伝える
  Serial.println("initialized");
  digitalWrite(PIN_LED, HIGH);
}

void loop() {
  SensorInfo sensors;
  OutputInfo outputs;

  // センサーの読み取り
#ifdef SERIAL_TEST
  sensors = ReadSensorsFromSerial();
#else
  sensors = ReadSensors();
#endif

  // 制御値の決定
  outputs = ModeLoop(sensors);

  // 出力
  ControlLoop(sensors, outputs);

  // 保存
  StorageLoop(sensors, outputs);

#ifndef DEBUG_MODE
  GPSData gps_data = { (int32_t)(outputs.next_mode)};
  Serial.println(sizeof(GPSData));
  data_transfer.Send(Serial, gps_data);
#endif

  // old_height 更新
  old_height = sensors.height;

  // 待機
  delay(WAIT_TIME_MS < sensors.dt_ms ? 0 : WAIT_TIME_MS - sensors.dt_ms);
  old_time_ms = sensors.time_ms;
  // Serial.println(sensors.dt_ms);
}

// センサーの出力を読み取る関数
SensorInfo ReadSensors() {
  SensorInfo sensors;
  // 201903機体でX軸が合う
  // const float rot_matrix[3][3] = {
  // {+0, +0, +1},
  // {+0, +1, +0},
  // {-1, +0, +0},
  // };
  const float t = radians(42.7);
  // 201903機体でX,Y,Z軸が合う
  const float rot_matrix[3][3] = {
    {+0, +0, +1},
    {+sinf(t), +cosf(t), +0},
    {-cosf(t), +sinf(t), +0},
  };

  float temp;
  enum RocketMode current_mode = GetCurrentMode();

  // 0初期化
  memset(&sensors, 0, sizeof(SensorInfo));

  // 時間[ms]の読み取り
  sensors.time_ms = millis();

  // 時間差分[ms]の計算
  sensors.dt_ms = sensors.time_ms - old_time_ms;

  // 加速度[g]の読み取り
  ImuGetMotion9(
    &sensors.ax, &sensors.ay, &sensors.az,
    &sensors.gx, &sensors.gy, &sensors.gz,
    &sensors.mx, &sensors.my, &sensors.mz);
  // 9軸センサーの回転
  RotateSensors(sensors, rot_matrix);

  // 気圧[hPa]の読み取り
  ImuReadPressure(&temp);
  sensors.pressure = lpf_pres.Push(temp);

  // 高度[m]の計算
  sensors.height = pressureToAltitudeMeters(sensors.pressure, STANDARD_ATMOSPHERE);

  // heightの微分[m/s]
  sensors.d_height =
    1e3 * (sensors.height - old_height) / (float)(sensors.time_ms - old_time_ms);

#ifdef DEBUG_MODE
sensors.d_height = 80;
#endif

#ifdef USE_GPS
  // 緯度経度の読み取り
  GpsReadPosition(&sensors.latitude, &sensors.longitude);
#endif

  // 点火検知時の高度
  sensors.height_at_ground = height_at_ground;

  // 姿勢推定
  filter.begin(1.0f / (1e-6 + sensors.dt_ms * 1e-3 ));
  if (current_mode == MODE_READY || current_mode == MODE_SETTING) {
    filter.update(
      sensors.gx, sensors.gy, sensors.gz,
      sensors.ax, sensors.ay, sensors.az,
      sensors.mx, sensors.my, sensors.mz
    );
  } else {
    filter.updateGyro(sensors.gx, sensors.gy, sensors.gz);
  }
  sensors.roll = filter.getRoll();
  sensors.pitch = filter.getPitch();
  sensors.yaw = filter.getYaw();

  // ピトー管
  sensors.pitot = analogRead(PIN_PITOT_TUBE);

  // temperature sensor
  sensors.motor_temp = analogRead(PIN_TEMP_SENSOR);

  // コマンドの読み取り
  SerialReadCommand(&sensors);

  return sensors;
}


// 実機のプログラム試験用
// シリアル通信で受け取ったデータから挙動を決定する
SensorInfo ReadSensorsFromSerial() {
  SensorInfo sensors;
  uint8_t data;
  static float old_height = 1000;

  // 0初期化
  memset(&sensors, 0, sizeof(SensorInfo));

  while (true) {
    if ( not Serial.available() ) {
      delay(1);
    } else {
      data = Serial.read();
      if (my_buff.GetLength() == 0) {
        if ( data == 0xAA )
          my_buff.Push(data);
      } else {
        my_buff.Push(data);
      }

      if (my_buff.GetLength() == 16) {
        uint8_t starts[2] = {0xAA, 0xBB};
        uint8_t ends[2] = {0xCC, 0xDD};
        uint8_t temp[16];
        uint32_t *t;
        float *h, *a;

        if ( my_buff.StartsWith(starts, 2) && my_buff.EndsWith(ends, 2) ) {
          my_buff.Copy(temp);
          t = (uint32_t*)(temp + 2);
          h = (float*)(temp + 6);
          a = (float*)(temp + 10);

          sensors.time_ms = *t;
          sensors.height = *h;
          sensors.ax = *a;
          sensors.ay = *a;
          sensors.az = *a;
          sensors.d_height = (sensors.height - old_height) / (float)(0.01);
          old_height = sensors.height;
          break;
        }

        my_buff.Clear();
      }
    }
  }

  sensors.dt_ms = 10;
  my_buff.Clear();
  return sensors;
}
