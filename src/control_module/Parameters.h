#ifndef PARAMETERS_HEADER_FILE
#define PARAMETERS_HEADER_FILE

// 時間でMODE_SETTINGからMODE_READYに遷移したい場合は
// TO_READY_TIMEをコメントインするとTO_READY_TIME[s]後に遷移する
//#define ENABLE_TO_/READY_BY_TIME
const float TO_READY_TIME = 15*60;

// 点火検知は加速度のノルムが
// ACCEL_IGNITION_THRESHOLD[g]を超えた状態を
// TIME_IGNITION_THRESHOLD[s]以上の時間継続した場合，点火と判断する
const float IGNITION_THRESHOLD_ACCEL = (3.0);
const float IGNITION_THRESHOLD_TIME = (0.1);

// エンジン燃焼時間
const float BURNING_TIME = (5.6);

// 1ループの待機時間[ms]
const uint32_t WAIT_TIME_MS = (10);

// PDゲイン
const float GAIN_ROLL_KP = 0.322144; // ハイブリッド
const float GAIN_ROLL_KD = 0.102542; // ハイブリッド
const float GAIN_PITCH_KP = 0.0; // ハイブリッド
const float GAIN_PITCH_KD = 0.0; // ハイブリッド

// parameter k for torque formula( T = k*v*v*x)
// T: torque, N m
// v: velocity. m/s,
// x: servo angle, deg
const float PARAM_ROLL_K = 2.01e-6;
const float PARAM_PITCH_K = 1.00e-4;

// 制御停止となる角度
const float THRESHOLD_ANGLE_TO_STOP_CONTROL = 30; // deg

// サーボ角の制限
const float LIMIT_OF_SERVO_ANGLE = 7;

// 加速度のバイアス
const float ACCEL_BIAS[3] = {0.0,0.0,0.35};

// ロールの出力の符号(+1/-1)で向きを変える
const float SIGN_OF_ROLL = 1;

// シリアル通信で挙動をテストする場合はこれをコメントイン
//#define SERIAL_TEST

// センサーデータなどをSerialに表示する場合
// 制御状態を確認するために速度を80 m/sとして扱いたい場合
// 上記の場合はこれをコメントインする
// TODO: コメントアウト
#define DEBUG_MODE

// GPSを使う場合はコメントイン
//#define USE_GPS

// GPSに接続したシリアル通信
//#define GPS_SERIAL Serial1

#endif
