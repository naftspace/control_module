#include "Utility.h"
#include "PinAssign.h"

// ロール制御のサーボ-A,Bのトリム
const float ANGLE_ROLL_SERVO_A = 90.0;
const float ANGLE_ROLL_SERVO_B = 90.0;
// タイマーの精度
const int LEDC_TIMER_BIT = 16;

// サーボ信号の１サイクル　50Hz:20ms
const int LEDC_SERVO_FREQ = 50;

const int LEDC_CHANNEL_A = 3; // サーボAのチャンネル
const int LEDC_CHANNEL_B = 10; // サーボAのチャンネル

const float SERVO_MIN_WIDTH_MS = 0.5;
const float SERVO_MAX_WIDTH_MS = 2.5;


void ControlInit() {
  // SERVO_Aの初期化
  ledcSetup(LEDC_CHANNEL_A, LEDC_SERVO_FREQ, LEDC_TIMER_BIT); // チャンネル，周期，16ビット精度で制御
  ledcAttachPin(PIN_ROLL_SERVO_A, LEDC_CHANNEL_A) ;           // ピンの登録

  ledcSetup(LEDC_CHANNEL_B, LEDC_SERVO_FREQ, LEDC_TIMER_BIT); // チャンネル，周期，16ビット精度で制御
  ledcAttachPin(PIN_ROLL_SERVO_B, LEDC_CHANNEL_B) ;           // ピンの登録

  ledcWrite(PIN_ROLL_SERVO_A, servo_pwm_count(ANGLE_ROLL_SERVO_A));
  ledcWrite(PIN_ROLL_SERVO_B, servo_pwm_count(ANGLE_ROLL_SERVO_B));
}

void ControlLoop(const SensorInfo sensors, const OutputInfo outputs) {
  // ロール制御用フィンの動作
  const float angle = constrain(outputs.angle, -LIMIT_OF_SERVO_ANGLE, LIMIT_OF_SERVO_ANGLE);
  switch (GetCurrentMode()) {
    case MODE_PITCH:
      ledcWrite(LEDC_CHANNEL_A, servo_pwm_count(ANGLE_ROLL_SERVO_A - SIGN_OF_ROLL * angle)) ;
      ledcWrite(LEDC_CHANNEL_B, servo_pwm_count(ANGLE_ROLL_SERVO_B + SIGN_OF_ROLL * angle) );
      break;
    default:
      ledcWrite(LEDC_CHANNEL_A, servo_pwm_count(ANGLE_ROLL_SERVO_A - angle) );
      ledcWrite(LEDC_CHANNEL_B, servo_pwm_count(ANGLE_ROLL_SERVO_B - angle) );
      break;
  }

  //  ledcWrite(LEDC_CHANNEL_A, servo_pwm_count((millis() / 1000) % 2 == 0 ? 75 : 105)) ;
  //  ledcWrite(LEDC_CHANNEL_B, servo_pwm_count((millis() / 1000) % 2 == 0 ? 105 : 75)) ;

  switch (outputs.next_mode) {
    case MODE_SETTING:
      BlinkLed(PIN_LED, 500, 50);
      break;
    case MODE_READY:
      BlinkLed(PIN_LED, 1000, 10);
      break;
    case MODE_BURNING:
      BlinkLed(PIN_LED, 100, 50);
      break;
    case MODE_ROLL:
      BlinkLed(PIN_LED, 500, 50);
      break;
    case MODE_PITCH:
      BlinkLed(PIN_LED, 1000, 50);
      break;
    case MODE_END:
      BlinkLed(PIN_LED, 3000, 50);
      break;
  }
}

void CheckServoTrim() {
  char buff[32];
  int index = 0;
  bool flag = true;
  float angle = 90;

  while (flag) {
    while (Serial.available()) {
      char c = Serial.read();
      buff[index] = c;
      index++;
      if (c == '\n') {
        buff[index] = '\0';
        angle = atof(buff);
        index = 0;
      }

      if ( angle > 360)flag = false;
    }
    ledcWrite(LEDC_CHANNEL_A, servo_pwm_count(angle) );
    ledcWrite(LEDC_CHANNEL_B, servo_pwm_count(angle) );
    delay(100);
  }
  ResetMicrocontroller();
}

void BlinkLed(int pin, int32_t period_ms, int32_t blink_ratio_percent) {
  int now_value = ((millis() % period_ms) * 100) / period_ms;
  digitalWrite(pin, now_value < blink_ratio_percent);
}

uint32_t servo_pwm_count(float v) {
  float vv = v / 180.0 ;
  uint32_t period_ms = (1000 / LEDC_SERVO_FREQ); // 20 ms
  uint32_t max_value = (1L << LEDC_TIMER_BIT);
  return (uint32_t)((max_value / period_ms ) * (SERVO_MIN_WIDTH_MS + vv * (SERVO_MAX_WIDTH_MS - SERVO_MIN_WIDTH_MS)) ) ;
}
