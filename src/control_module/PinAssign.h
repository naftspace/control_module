#ifndef PIN_ASSIGN_HEADER_FILE
#define PIN_ASSIGN_HEADER_FILE

#include<pins_arduino.h>

// 初期化後に点灯させるLEDのピン
const int PIN_LED = 32;

const int PIN_TEMP_SENSOR = 26;
const int PIN_PITOT_TUBE = 27;

// 制御フィンのサーボピン
const int PIN_ROLL_SERVO_A = 17;
const int PIN_ROLL_SERVO_B = 5;

// Serial1のRX,TXピン
// const int PIN_GPS_RX = 16;
// const int PIN_GPS_TX = 17;


// ESP32ではSDA=21,SCL=22

#endif
