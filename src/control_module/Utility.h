#ifndef UTILITY_HEADER_FILE
#define UTILITY_HEADER_FILE

#include <Arduino.h>
#include <inttypes.h>
#include <math.h>
#include "EEPROM.h"
#include "SD_MMC.h"
#include "helper_3dmath.h" // https://github.com/joshsh/laboratory/tree/master/mods/i2cdevlib/MPU9150

// 標準大気圧[hPa]
#define STANDARD_ATMOSPHERE (1013.25)

// ロケットの状態を表す列挙型
enum RocketMode {
  MODE_SETTING = 0,
  MODE_READY = 1,
  MODE_BURNING = 2,
  MODE_ROLL = 3,
  MODE_PITCH = 4,
  MODE_END = 5,
};

// Serialで送られてくる信号の解析結果
enum CommandResult {
  RESULT_TO_SETTING,  // MODE_SETTINGに強制遷移する
  RESULT_TO_READY,    // MODE_READYに強制遷移する
  RESULT_TO_ROLL,    // MODE_ROLLに強制遷移する
  RESULT_TO_PITCH,    // MODE_PITCHに強制遷移する
  RESULT_RESET,       // リセットする
  RESULT_CHANGE_1ST,  // MODE_SETTINGの時第1段階モータの開閉を変更する
  RESULT_CHANGE_2ND,  // MODE_SETTINGの時第2段階モータの開閉を変更する
  RESULT_BIAS,        // センサーのキャリブレーションをし，リセット
  RESULT_ACCEL_BIAS,  // ACCEL_BIASのためのコマンド
  RESULT_STOP_CONTROL,// 制御停止命令
  RESULT_CHANGE_TRIM, // サーボのトリム変更
  RESULT_NONE,        // 意味を解析できなかった場合
};

// センサーの値をまとめるSensorInfo構造体
typedef struct _SensorInfo {
  uint32_t time_ms;       // 時間[ms]
  uint32_t dt_ms;         // 時間差[ms]
  float ax, ay, az;       // 加速度ベクトル[g]
  float gx, gy, gz;       // 角速度ベクトル[dps]
  float mx, my, mz;       // 磁気ベクトル[Gauss]
  float pressure;         // 圧力[hPa]
  float height;           // 1013.25 hPaを0 mとした場合の高度[m]
  float d_height;         // heightの微分[m/s]
  float latitude;         // 緯度
  float longitude;        // 経度
  float height_at_ground; // 点火検知時の高度
  float roll;             // ロール角[deg]
  float pitch;            // ピッチ角[deg]
  float yaw;              // ヨー角[deg]
  int pitot;              // ピトー管の出力(生)
  int motor_temp;  // output value of temperature sensor(raw)

  enum CommandResult cmd; // 命令
} SensorInfo;

// 出力をまとめるOutputInfo構造体
typedef struct _OutputInfo {
  enum RocketMode next_mode;  // 次状態
  float target_torque;        // 入力トルク
  float angle;                // サーボモータ角度[deg]
} OutputInfo;

// 磁気，ジャイロのバイアスを格納する構造体
typedef struct {
  float gx, gy, gz;
  float mx, my, mz;
} SensorBias;

// EEPROMにデータを書き込む関数
template <typename T>
void write_to_eeprom(T data) {
  char *buff = (char*)(&data);

  for (size_t i = 0 ; i < sizeof(T) ; i++ ) {
    EEPROM.write(i, buff[i]);
  }
}

// EEPROMからデータを読み込む関数
template <typename T>
T read_from_eeprom() {
  T data;
  char *buff = (char*)(&data);

  for (size_t i = 0 ; i < sizeof(T) ; i++ ) {
    buff[i] = EEPROM.read(i);
  }

  return data;
}

// SDからデータを読み込む関数
template <typename T>
T read_from_sd(const char *filename) {
  T *data = (T*)calloc(1,sizeof(T));
  char *buff = (char*)data;

  // ファイルオープン
  File dataFile = SD_MMC.open(filename, FILE_READ);

  if( not dataFile || sizeof(T) > dataFile.size() ){
    return *data;
  }

  for(int i = 0 ; i < sizeof(T) ; i++ ){
    buff[i] = dataFile.read();
  }

  dataFile.close();

  return *data;
}

// EEPROMにデータを書き込む関数
template <typename T>
void write_to_sd(T data,const char *filename) {
  char *buff = (char*)(&data);
  File dataFile = SD_MMC.open(filename, FILE_WRITE);

  if( not dataFile ){
    return;
  }

  for (size_t i = 0 ; i < sizeof(T) ; i++ ) {
    dataFile.write(buff[i]);
  }
  dataFile.close();
}

// xを[xmin,xmax]から[ymin,ymax]になるように線形補間した後サチる
int16_t map_constrain(int16_t x, int16_t xmin, int16_t xmax, int16_t ymin, int16_t ymax) {
  int16_t y = map(x, xmin, xmax, ymin, ymax);
  return constrain(y, ymin, ymax);
}

// pressure[hPa]をpres0を基準にして高度[m]に変換する関数
float pressureToAltitudeMeters(float pres, float pres0) {
  return (1 - pow(pres / pres0, 0.190263)) * 44330.8;
}

// 角度theta[rad]を-pi～+piに押し込む
float normalize_angle_rad(const float theta) {
  // TODO: 剰余とか使って高速化
  return atan2(sin(theta), cos(theta));
}

// 角度theta[deg]を-180～+180に押し込む
float normalize_angle_deg(const float theta) {
  return degrees(normalize_angle_rad(radians(theta)));
}

void RotateSensors(SensorInfo & sensors, const float rot_matrix[3][3]) {
  float tx, ty, tz;

  tx = rot_matrix[0][0] * sensors.ax + rot_matrix[0][1] * sensors.ay + rot_matrix[0][2] * sensors.az;
  ty = rot_matrix[1][0] * sensors.ax + rot_matrix[1][1] * sensors.ay + rot_matrix[1][2] * sensors.az;
  tz = rot_matrix[2][0] * sensors.ax + rot_matrix[2][1] * sensors.ay + rot_matrix[2][2] * sensors.az;
  sensors.ax = tx; sensors.ay = ty; sensors.az = tz;

  tx = rot_matrix[0][0] * sensors.gx + rot_matrix[0][1] * sensors.gy + rot_matrix[0][2] * sensors.gz;
  ty = rot_matrix[1][0] * sensors.gx + rot_matrix[1][1] * sensors.gy + rot_matrix[1][2] * sensors.gz;
  tz = rot_matrix[2][0] * sensors.gx + rot_matrix[2][1] * sensors.gy + rot_matrix[2][2] * sensors.gz;
  sensors.gx = tx; sensors.gy = ty; sensors.gz = tz;

  tx = rot_matrix[0][0] * sensors.mx + rot_matrix[0][1] * sensors.my + rot_matrix[0][2] * sensors.mz;
  ty = rot_matrix[1][0] * sensors.mx + rot_matrix[1][1] * sensors.my + rot_matrix[1][2] * sensors.mz;
  tz = rot_matrix[2][0] * sensors.mx + rot_matrix[2][1] * sensors.my + rot_matrix[2][2] * sensors.mz;
  sensors.mx = tx; sensors.my = ty; sensors.mz = tz;
}

// マイコンをリセットする関数
void ResetMicrocontroller() {
  // Teensy 3.5(ARMプロセッサ)
  // http://qiita.com/edo_m18/items/a7c747c5bed600dca977
  //  asm volatile ("B _start");

  // A」
  ESP.restart();
}

// create a quaternion from Euler angles
// copied from ArduPilot's library
// roll,pitch,yaw are radian
Quaternion from_euler(float roll, float pitch, float yaw){
    const float cr2 = cosf(roll*0.5f);
    const float cp2 = cosf(pitch*0.5f);
    const float cy2 = cosf(yaw*0.5f);
    const float sr2 = sinf(roll*0.5f);
    const float sp2 = sinf(pitch*0.5f);
    const float sy2 = sinf(yaw*0.5f);

    float q1 = cr2*cp2*cy2 + sr2*sp2*sy2;
    float q2 = sr2*cp2*cy2 - cr2*sp2*sy2;
    float q3 = cr2*sp2*cy2 + sr2*cp2*sy2;
    float q4 = cr2*cp2*sy2 - sr2*sp2*cy2;

    return Quaternion(q1,q2,q3,q4);
}

// get euler roll angle
// copied from ArduPilot's library
// roll,pitch,yaw are radian
void to_euler(Quaternion q,float & roll,float & pitch,float & yaw){
  q.normalize();

  const float q1 = q.w;
  const float q2 = q.x;
  const float q3 = q.y;
  const float q4 = q.z;

  roll = (atan2f(2.0f*(q1*q2 + q3*q4), 1.0f - 2.0f*(q2*q2 + q3*q3)));
  pitch = asin(2.0f*(q1*q3 - q4*q2));
  yaw = atan2f(2.0f*(q1*q4 + q2*q3), 1.0f - 2.0f*(q3*q3 + q4*q4));
}

// 誤差クォータニオンの計算を行う関数
Quaternion calc_error_quaternion(Quaternion target,Quaternion attitude){
  target.normalize();
  attitude.normalize();

  return target.getProduct(attitude.getConjugate());
}

// N元連立線形方程式を解く
template<int N>
void solve(const float M[N][N], float x[N], const float b[N])
{
  float A[N][N];
  float B[N];
  float temp;

  // コピー
  for (int i = 0 ; i < N ; i++ ) {
    B[i] = b[i];
    for (int j = 0 ; j < N ; j++ ) {
      A[i][j] = M[i][j];
    }
  }
  for (int i = 0 ; i < N ; i++ ) {
    // ピボット選択
    for (int j = i + 1 ; j < N ; j++ ) {
      if ( fabs(A[i][j]) >= 1e-8f )
        break;

      for (int k = 0 ; k < N ; k++ )
        A[i][k] += A[j][k];
      B[i] += B[j];
    }

    // 対角成分を1に
    temp = A[i][i];
    for (int j = i ; j < N ; j++ )
      A[i][j] /= temp;
    B[i] /= temp;

    // 前進消去
    for (int j = i + 1 ; j < N ; j++ ) {
      temp = A[j][i];

      for (int k = i ; k < N ; k++ )
        A[j][k] -= temp * A[i][k];
      B[j] -= temp * B[i];
    }
  }

  // 後進消去
  for (int i = N - 1 ; i >= 0 ; i-- ) {
    for (int j = i - 1 ; j >= 0 ; j-- ) {
      B[j] -= A[j][i] * B[i];
    }
  }

  for (int i = 0 ; i < N ; i++ )
    x[i] = B[i];
}

template <typename T>
T sign(T a) {
  return a > 0 ? 1 : a < 0 ? -1 : 0;
}


// RocketMode列挙型を文字列に変換する関数
String to_string(const enum RocketMode mode) {
  switch (mode) {
    case MODE_SETTING:
      return "MODE_SETTING";
    case MODE_READY:
      return "MODE_READY";
    case MODE_BURNING:
      return "MODE_BURNING";
    case MODE_ROLL:
      return "MODE_ROLL";
    case MODE_PITCH:
      return "MODE_PITCH";
    case MODE_END:
      return "MODE_END";
    default:
      return "MODE_UNKNOWN";
  }
}

// CommandResult列挙型を文字列に変換する関数
String to_string(const enum CommandResult result) {
  switch (result) {
    case RESULT_TO_SETTING:
      return "RESULT_TO_SETTING";
    case RESULT_TO_READY:
      return "RESULT_TO_READY";
    case RESULT_TO_PITCH:
      return "RESULT_TO_PITCH";
    case RESULT_TO_ROLL:
      return "RESULT_TO_ROLL";
    case RESULT_RESET:
      return "RESULT_RESET";
    case RESULT_CHANGE_1ST:
      return "RESULT_CHANGE_1ST";
    case RESULT_CHANGE_2ND:
      return "RESULT_CHANGE_2ND";
    case RESULT_NONE:
      return "RESULT_NONE";
    default:
      return "RESULT_UNKNOWN";
  }
}

// センサーの値をStringに変換する関数
String to_string(const SensorInfo sensors, const bool check_motion = false) {
  String dataString = "";

  if ( not check_motion ) {
    dataString += String(sensors.time_ms);
    dataString += ",\t";
    dataString += String(sensors.dt_ms);
    dataString += ",\t";
  }

  dataString += String(sensors.ax);
  dataString += ",\t";
  dataString += String(sensors.ay);
  dataString += ",\t";
  dataString += String(sensors.az);
  dataString += ",\t";
  dataString += String(sensors.gx);
  dataString += ",\t";
  dataString += String(sensors.gy);
  dataString += ",\t";
  dataString += String(sensors.gz);
  dataString += ",\t";
  dataString += String(sensors.mx);
  dataString += ",\t";
  dataString += String(sensors.my);
  dataString += ",\t";
  dataString += String(sensors.mz);
  dataString += ",\t";

  if ( not check_motion ) {
    dataString += String(sensors.pressure);
    dataString += ",\t";
    dataString += String(sensors.height);
    dataString += ",\t";
    dataString += String(sensors.d_height);
    dataString += ",\t";

    dataString += String(sensors.latitude);
    dataString += ",\t";
    dataString += String(sensors.longitude);
    dataString += ",\t";

    dataString += String(sensors.height_at_ground);
    dataString += ",\t";
  }

  dataString += String(sensors.roll);
  dataString += ",\t";
  dataString += String(sensors.pitch);
  dataString += ",\t";
  dataString += String(sensors.yaw);
  dataString += ",\t";

  if ( not check_motion ) {
    dataString += String(sensors.pitot);
    dataString += ",\t";
    dataString += String(sensors.motor_temp);
    dataString += ",\t";

    dataString += to_string(sensors.cmd);
    dataString += ",\t";
  }

  return dataString;
}

// 出力の値をStringに変換する関数
String to_string(const OutputInfo outputs) {
  String dataString = "";

  dataString += to_string(outputs.next_mode);
  dataString += ",\t";
  dataString += String(outputs.target_torque, 10);
  dataString += ",\t";
  dataString += String(outputs.angle);
  dataString += ",\t";

  return dataString;
}

typedef struct {
  int32_t next_mode;
} GPSData;

#endif
