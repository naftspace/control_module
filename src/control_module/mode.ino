#include "ModeClasses.h"
#include "Utility.h"

// 現在の状態
enum RocketMode current_mode;
// モードクラス
ModeBase* modes[6];

void ModeInit() {
  modes[0] = (ModeBase*)new ModeSetting();
  modes[1] = (ModeBase*)new ModeReady();
  modes[2] = (ModeBase*)new ModeBurning();
  modes[3] = (ModeBase*)new ModeRoll();
  modes[4] = (ModeBase*)new ModePitch();
  modes[5] = (ModeBase*)new ModeEnd();

  // 初期状態MODE_SETTINGの設定
  ModeChange(MODE_SETTING, millis());
}

OutputInfo ModeLoop(const SensorInfo sensors) {
  OutputInfo outputs;

  // 0初期化
  memset(&outputs, 0, sizeof(OutputInfo));

  // MODE_SETTING,MODE_READYでは地上高度を随時更新
  if ( current_mode == MODE_SETTING
       || current_mode == MODE_READY )
    height_at_ground = sensors.height;

  // 出力を決定
  outputs = modes[(int)current_mode]->Control(sensors);

  // MODE_SETTINGに移行するように信号が送られてきた場合はnext_modeをそれに
  if ( sensors.cmd == RESULT_TO_SETTING ) {
    outputs.next_mode = MODE_SETTING;
  }

  // MODE_READYに移行するように信号が送られてきた場合はnext_modeをそれに
  if ( sensors.cmd == RESULT_TO_READY  ) {
    outputs.next_mode = MODE_READY;
  }
  
  // MODE_PITCHに移行するように信号が送られてきた場合はnext_modeをそれに
  if ( sensors.cmd == RESULT_TO_PITCH  ) {
    outputs.next_mode = MODE_PITCH;
  }
  
  // MODE_ROLLに移行するように信号が送られてきた場合はnext_modeをそれに
  if ( sensors.cmd == RESULT_TO_ROLL  ) {
    outputs.next_mode = MODE_ROLL;
  }

  // 状態が変化した場合はModeChangeを呼び出し次状態を初期化
  if ( outputs.next_mode != current_mode ) {
    ModeChange(outputs.next_mode, sensors.time_ms);
  }

  return outputs;
}

void ModeChange(enum RocketMode next_mode, uint32_t time_ms) {
  // 次の状態を初期化する
  modes[(int)next_mode]->Reset();
  modes[(int)next_mode]->SetStartedTime(time_ms);

  // 現在の状態を新しい物にする
  current_mode = next_mode;
}

enum RocketMode GetCurrentMode() {
  return current_mode;
}
