#ifndef PID_HEADER_FILE
#define PID_HEADER_FILE

class PID {
  public:
    PID(float kp, float ki, float kd) {
      setKp(kp);
      setKi(ki);
      setKd(kd);
      init();
      sign_ = 1;
    }

    void init() {
      isFirst_ = true;
      old_error_ = 0;
      integral_ = 0;
      old_d_error_ = 0;
    }

    void inverse() {
      sign_ *= -1;
    }

    void setKp(float kp) {
      kp_ = kp;
    }

    void setKi(float ki) {
      ki_ = ki;
    }

    void setKd(float kd) {
      kd_ = kd;
    }

    float output(const float error, const float dt) {
      const float d_error = 
        isFirst_ ? 0 : (0.8f * old_d_error_ + 0.2f * (error - old_error_) / dt);

      isFirst_ = false;
      integral_ += error * dt;
      const float ret = kp_ * error + ki_ * integral_ + kd_ * d_error;

      old_error_ = error;
      old_d_error_ = d_error;

      return ret * sign_;
    }

  private:


    float kp_, ki_, kd_;
    float integral_, old_error_, old_d_error_;
    int sign_;
    bool isFirst_;
};

#endif

