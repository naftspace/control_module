#include <SPI.h>
#include <SD.h>

// Beetleは4
const int chipSelect = 4;
// バッファサイズ
const size_t BUFFER_SIZE = 64;
// バッファ
char buff[BUFFER_SIZE + 1 ];
// バッファに入ってるデータの数
size_t index = 0;
// ファイル名
char filename[32];
// LEDのピン
const int PIN_LED = 13;

void setup() {
  int n_logdata = 0;

  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, LOW);

  Serial.begin(19200);
  Serial1.begin(19200);
  Serial.print("Initializing SD card...");
  delay(100);

  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    delay(1000);
    reset();
  }
  delay(100);

  // ファイル名が被らないようにする
  do {
    sprintf(filename, "%04d.txt", n_logdata);
    if ( SD.exists(filename) == false )
      break;
    n_logdata++;
  } while (true);

  Serial.println("card initialized.");
  Serial.print("target filename is :");
  Serial.println(filename);

  //  write_data(filename, "hello", 5);
  digitalWrite(PIN_LED, HIGH);
}

void loop() {
  static bool is_first = true;

  while (Serial1.available() > 0) {
    char c = Serial1.read();
    buff[index] = c;
    index++;

    if ( index == BUFFER_SIZE ) {
      write_data(filename, buff, BUFFER_SIZE);
      index = 0;
    }
  }

  if (is_first && millis() > 5000 ) {
    is_first = false;
    Serial1.print("TO_READY\r\n");
  }
}
//配列の変数名は配列の最初の要素のアドレスを示す
void write_data(char *fname, char *str, size_t str_length) {
  File dataFile = SD.open(fname, FILE_WRITE);
  digitalWrite(PIN_LED, LOW);

  str[str_length] = '\0';
  if (dataFile) {
    dataFile.print(str);
    dataFile.close();
    Serial.print("data wrote: ");
    Serial.println(str);
  } else {
    Serial.println("error opening datalog.txt");
  }
  digitalWrite(PIN_LED, HIGH);
}

void reset() {
  asm volatile ( " jmp 0" );
}








