#ifndef UTILITY_HEADER_FILE
#define UTILITY_HEADER_FILE

#include <Arduino.h>
#include <inttypes.h>
#include <math.h>

// ロケットの状態を表す列挙型の値
const int16_t MODE_SETTING = 0;
const int16_t MODE_READY = 1;
const int16_t MODE_BURNING = 2;
const int16_t MODE_FREEFALL = 3;
const int16_t MODE_HALF_OPEN = 4;
const int16_t MODE_FULL_OPEN = 5;

// Serialで送られてくる信号の解析結果
const int16_t RESULT_TO_SETTING = 0;  // MODE_SETTINGに強制遷移する
const int16_t RESULT_TO_READY = 1;    // MODE_READYに強制遷移する
const int16_t RESULT_RESET = 2;       // リセットする
const int16_t RESULT_CHANGE_1ST = 3;  // MODE_SETTINGの時第1段階モータの開閉を変更する
const int16_t RESULT_CHANGE_2ND = 4;  // MODE_SETTINGの時第2段階モータの開閉を変更する
const int16_t RESULT_NONE = 5;        // 意味を解析できなかった場合


// センサーの値をまとめるSensorInfo構造体
typedef struct _NanoData {
  uint32_t time_ms;       // 時間[ms]
  float accel_norm;       // 加速度ノルム
  float height;           // 高度[m]
  float height_at_ground; // 地上の高度
  float battery_servo;    // サーボ用電源電圧
  float battery_arduino;  // マイコン用電源電圧
  int16_t mode;   // 現在の状態
  int16_t cmd;    // 命令
} NanoData;

typedef struct {
  int32_t next_mode;
} GPSData;

// RocketMode列挙型を文字列に変換する関数
inline String rocket_mode_to_string(const int16_t mode) {
  switch (mode) {
    case MODE_SETTING:
      return "MODE_SETTING";
    case MODE_READY:
      return "MODE_READY";
    case MODE_BURNING:
      return "MODE_BURNING";
    case MODE_FREEFALL:
      return "MODE_FREEFALL";
    case MODE_HALF_OPEN:
      return "MODE_HALF_OPEN";
    case MODE_FULL_OPEN:
      return "MODE_FULL_OPEN";
    default:
      return "MODE_UNKNOWN";
  }
}

// CommandResult列挙型を文字列に変換する関数
inline String command_result_to_string(const int16_t result) {
  switch (result) {
    case RESULT_TO_SETTING:
      return "RESULT_TO_SETTING";
    case RESULT_TO_READY:
      return "RESULT_TO_READY";
    case RESULT_RESET:
      return "RESULT_RESET";
    case RESULT_CHANGE_1ST:
      return "RESULT_CHANGE_1ST";
    case RESULT_CHANGE_2ND:
      return "RESULT_CHANGE_2ND";
    case RESULT_NONE:
      return "RESULT_NONE";
    default:
      return "RESULT_UNKNOWN";
  }
}

// NanoDataをStringにする関数
inline String to_string(const NanoData sensors) {
  String dataString = "";

  dataString.concat(String(sensors.time_ms));
  dataString.concat(",\t");
  dataString.concat(String(sensors.accel_norm));
  dataString.concat(",\t");
  dataString.concat(String(sensors.height));
  dataString.concat(",\t");
  dataString.concat(String(sensors.height_at_ground));
  dataString.concat(",\t");
  dataString.concat(rocket_mode_to_string(sensors.mode));
  dataString.concat(",\t");
  dataString.concat(command_result_to_string(sensors.cmd));
  dataString.concat(",\t");

  return dataString;
}

// GPSDataをStringにする関数
inline String to_string(const GPSData gps) {
  String result = "";
//  result.concat(String(gps.latitude, 5) + "\t,");
//  result.concat(String(gps.longitude, 5) + "\t,");
  result.concat(rocket_mode_to_string(gps.next_mode) + "\t,");
  return result;
}

// マイコンをリセットする関数
void ResetMicrocontroller() {
  // Teensy 3.5(ARMプロセッサ)
  // http://qiita.com/edo_m18/items/a7c747c5bed600dca977
  asm volatile ("B _start");

  // Arduino Uno他(AVRマイコン)
  //  asm volatile ("  jmp 0");
}

#endif
