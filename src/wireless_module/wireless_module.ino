#include "Utility.h"
#include "DataTransfer.h"
#include "Parameters.h"

void setup() {
  Serial.begin(19200);
  ESP32Init();
  NanoInit();
  Im920Init();
  StorageInit();
  GpsInit();
  pinMode(PIN_CHECK_LED, OUTPUT);
  delay(100);
  Serial.println("initialized");
  delay(100);
  SERIAL_NANO.flush();
  SERIAL_ESP32.flush();
}

void loop() {
  double latitude, longitude;
  bool flag = NanoLoop();

  ESP32Loop();

  GpsReadBuffer();

  const GPSData gps_data = GetGPSData();

  const NanoData nano_data = GetNanoData();

  GpsReadPosition(&latitude, &longitude);

  Im920ReadAndSend();

  Im920Send(nano_data, gps_data, latitude, longitude);

  if (flag)
    StorageLoop(nano_data, latitude, longitude);

  digitalWrite(PIN_CHECK_LED, (millis() / 500) % 2 == 0 ? HIGH : LOW);
  //  delay(20);
}
