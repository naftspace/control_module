
DataTransfer<NanoData> nano_data_transfer(32);

// Nanoから送られてきた情報
NanoData local_nano_data = { 0, 0.0, 0.0, 0.0, -1, 0};

void NanoInit() {
  SERIAL_NANO.begin(19200);
}

bool NanoLoop() {
  bool flag = false;

  while (SERIAL_NANO.available()) {
    char c = SERIAL_NANO.read();
//    Serial.print(c);
    if ( nano_data_transfer.Encode(c) ) {
      local_nano_data = nano_data_transfer.GetResult();
      flag = true;
#ifdef DEBUG_MODE
      Serial.println(to_string(local_nano_data));
#endif
    }
  }

  return flag;
}

NanoData GetNanoData() {
  return local_nano_data;
}
