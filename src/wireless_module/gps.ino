#include "TinyGPS++.h" // https://github.com/mikalhart/TinyGPSPlus


TinyGPSPlus tinygps;

void GpsInit() {
  SERIAL_GPS.begin(9600);
  Serial.println("Initializing gps...ok");
}

void GpsReadBuffer() {
  uint8_t temp;

  // バッファに何かあったら読み取り解析
  while (SERIAL_GPS.available()) {
    temp = SERIAL_GPS.read();
    //    Serial.print((char)temp);
    tinygps.encode(temp);
  }
}

// 緯度経度を読み取る関数
void GpsReadPosition(double *lati, double *lon) {
  GpsReadBuffer();
  *lati = tinygps.location.lat();
  *lon = tinygps.location.lng();
}

