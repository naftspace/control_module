
DataTransfer<GPSData> esp32_data_transfer(32);
GPSData local_gps_data = { -1};

void ESP32Init() {
  SERIAL_ESP32.begin(19200);
}

bool ESP32Loop() {
  bool flag = false;

  while (SERIAL_ESP32.available()) {
    char c = SERIAL_ESP32.read();
    //    Serial.println((int)c);
    if ( esp32_data_transfer.Encode(c) ) {
      local_gps_data = esp32_data_transfer.GetResult();
      flag = true;
#ifdef DEBUG_MODE
      Serial.println(to_string(local_gps_data));
#endif
    }
  }

  return flag;
}

GPSData GetGPSData() {
  return local_gps_data;
}
