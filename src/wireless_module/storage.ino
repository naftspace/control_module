#include <SD.h>
#include <SPI.h>

// 詳細求む
const int chipSelect = BUILTIN_SDCARD;
// SDカードを開けたらtrue
bool is_sd_open = true;
// 保存するログデータの番号
int n_logdata = 0;
// ファイル名を格納する変数
char sd_filename[64];

void StorageInit() {
  Serial.println("Initializing SD card...");

  if (not SD.begin(chipSelect)) {
    // 初期化に失敗した場合
    Serial.println("Card failed, or not present");
    is_sd_open = false;
    ResetMicrocontroller();
  } else {
    // 初期化に成功した場合はファイル名がかぶらないようなファイルを作る
    is_sd_open = true;
    do {
      sprintf(sd_filename, "%04d.txt", n_logdata);
      if ( SD.exists(sd_filename) == false )
        break;
      n_logdata++;
    } while (true);
  }

  Serial.println("card initialized.");
}

void StorageLoop(const NanoData nano, const double latitude, const double longitude) {
  String str = to_string(nano) +  String(latitude, 5) + "\t," + String(longitude, 5);

#ifdef DEBUG_MODE
  // 必要ならシリアルに表示
  Serial.println(str);
#else
  // do something
#endif

  if ( is_sd_open == false )
    return;

  // ファイルオープン
  File dataFile = SD.open(sd_filename, FILE_WRITE);

  if (dataFile) {
    // うまく開けていたら文字列を書き込む
    dataFile.println(str);
    dataFile.close();
    //        Serial.println("wrote");
  }
  else {
    //    Serial.println("error opening datalog.txt")
    //    Serial.println("miss");
  }
}


