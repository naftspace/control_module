#include <HardwareSerial.h>
#include "Utility.h"
#include "DataBuffer.h"
#include "Parameters.h"

const size_t IM920_BUFFER_SIZE = 80;            // バッファサイズ
const uint32_t IM920_SEND_INTERVAL_MS = 3000;   // im920に送信するデータの間隔
DataBuffer im920_buffer(IM920_BUFFER_SIZE);     // im920用のバッファ

void Im920Init() {
  SERIAL_IM920.begin(19200);
  delay(100);

  // ECIOモードにする(デフォルトでDCIO)
  // 不揮発メモリに書き込み済みだが一応
  SERIAL_IM920.println("ECIO");
}

// 座標・状態をPCに送信する
// sensors: 現在のセンサーの情報
void Im920Send(const NanoData nano, const GPSData gps, const double latitude, const double longitude) {
  static int32_t last_time = 0L;
  const uint32_t now_time = millis();
  char buff[IM920_BUFFER_SIZE];

  if ( now_time - last_time > IM920_SEND_INTERVAL_MS ) {
    // 時間がIM920_SEND_INTERVAL_MSを超えていた場合送信する
    String data = "TXDA ";
    data += "N" + String(longitude, 5) + ",";
    data += "E" + String(latitude, 5) + ",";
    data += String(nano.height - nano.height_at_ground, 0) + "m,";
    data += String(min(nano.battery_servo, nano.battery_arduino), 1) + "V,";
    data += rocket_mode_to_string(nano.mode).substring(5) + ",";
    data += rocket_mode_to_string(gps.next_mode).substring(5);
    data.toCharArray(buff, IM920_BUFFER_SIZE);

    SERIAL_IM920.println(buff);

#ifdef DEBUG_MODE
    Serial.println(buff);
#endif

    last_time = now_time;
  }
}

// im920から送られてきたデータが無いか確認し，あったらNanoやESP32におくる
void Im920ReadAndSend() {
  char buff[IM920_BUFFER_SIZE];
  char *main_str;
  String data;

  while (SERIAL_IM920.available()) {
    im920_buffer.Push(SERIAL_IM920.read());

    // 改行されていた場合
    if ( im920_buffer.EndsWith((uint8_t*)"\r\n", 2) ) {
      im920_buffer.Copy((uint8_t*)buff);    // 1行読み取る
      im920_buffer.Clear();
      const int n_buff = strlen(buff);      // 長さをカウント

      // 一部の文字列は無視する
      if (n_buff < 12 || strcmp(buff, "OK\r\n") == 0 || strcmp(buff, "NG\r\n") == 0 )
        return;

      main_str = buff + 12;

#ifdef DEBUG_MODE
      Serial.print("buff = ");
      Serial.print(buff);
      Serial.print("main_str = ");
      Serial.print(main_str);
#endif

      SERIAL_NANO.print(main_str);
      SERIAL_ESP32.print(main_str);

    }
  }
}
