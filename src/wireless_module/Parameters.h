#ifndef PARAMETERS_HEADER_FILE
#define PARAMETERS_HEADER_FILE

#define SERIAL_IM920 Serial4
#define SERIAL_NANO Serial3
#define SERIAL_ESP32 Serial2
#define SERIAL_GPS Serial5

#define DEBUG_MODE


const int PIN_CHECK_LED = 13;

#endif
